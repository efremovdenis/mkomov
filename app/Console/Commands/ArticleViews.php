<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Article;
use App\Models\ArticleView;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ArticleViews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'article:views';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update field `articles`.`r_views` and delete old views.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $query = ArticleView::select('article_id', DB::raw('count(*) as views'))->whereDate('created_at', '<', Carbon::today());
        $oldViews = $query->groupBy('article_id')->get();
        foreach($oldViews as $row) { // Optimize: use INSERT ON dublicate UPDATE.
            $article = Article::find($row->article_id);
            if(is_null($article)) continue;
            $article->update(['r_views' => $row->views + $article->r_views]);
        }
        $query->delete();
        $this->info("Articles updated. Old views deleted.");
    }
}
