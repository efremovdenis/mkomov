<?php
namespace App\Traits;

trait CanGetTableName {
    public static function getTableName() {
        return with(new static)->getTable();
    }
}