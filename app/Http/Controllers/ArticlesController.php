<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Tag;
use App\Models\Article;
use App\Models\ArticleLike;
use App\Models\ArticleView;
use Carbon\Carbon;

class ArticlesController extends Controller {
    public function index() {
        $articles = Article::latest('id')->limit(config('app.articles_last_amount'))->get();
        return view('articles.index', compact('articles'));
    }
    public function list() {
        $articles = Article::latest('id')->paginate(config('app.articles_per_page'));
        $title = 'Show all articles.';
        return view('articles.list', compact('articles', 'title'));
    }
    public function tag($tagName) {
        $tag = Tag::where('name', $tagName)->firstOrFail();        
        $title = 'Show articles via tag: '.$tag->name;
        $articles = $tag->articles()->latest('id')->paginate(config('app.articles_per_page'));
        return view('articles.list', compact('articles', 'title'));
    }
    public function view(Request $request, $slug) {
        $article = Article::where('slug', mb_strtolower($slug))->firstOrFail();
        return view('articles.show', compact('article'));
    }
    
}
