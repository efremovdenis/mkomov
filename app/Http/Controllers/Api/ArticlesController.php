<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\ArticleLike;
use App\Models\ArticleView;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticlesController extends Controller
{
    public function view(Request $request) {
        $article = Article::findOrFail((int) $request->get('article_id'));
        
        $view = new ArticleView();
        $article->views()->save($view);
        
        if($view->exists) return response()->json($article->total_views + 1, 200);
        else return response()->json('fail', 500);
    }
    
    public function like(Request $request) {
        $article = Article::findOrFail((int) $request->get('article_id'));

        $like = $article->likes()->where('ip_address', ip2long($request->ip()))->first();
        if(is_null($like)) {
            $like = new ArticleLike(['ip_address' => $request->ip()]);
            $article->likes()->save($like);
            return response()->json($article->total_likes + 1, 200);
        } else {
            // Delete like from DB.
            DB::table('article_likes')->where('article_id', $article->id)->where('ip_address', ip2long($request->ip()))->delete();
            return response()->json($article->total_likes - 1, 200);
        }
        
    }
}
