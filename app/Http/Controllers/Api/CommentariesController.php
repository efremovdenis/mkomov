<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommentaryRequest;

use App\Models\Article;
use App\Models\Commentary;

class CommentariesController extends Controller
{
    public function store(CommentaryRequest $request) {  
        sleep(10);             
        $article = Article::find((int) $request->get('article_id'));
        if(is_null($article)) return response(['Article not found'], 404);
        $commentary = new Commentary($request->validated());
        $article->commentaries()->save($commentary);
        return response()->json('success', 200);
    }
}
