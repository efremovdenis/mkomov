<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use App\Models\Tag;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    public function create() {
        return view('articles.admin.create');
    }
    public function edit($id) {
        $article = Article::find($id);
        if(is_null($article)) return redirect(route('index'))->with('danger', 'Article not found');

        return view('articles.admin.edit', compact('article'));
    }

    public function store(ArticleRequest $request) {
        $article = Article::create($request->except('tags'));
        $this->manageTags($article, $request->get('tags'));
        
        return redirect(route('articles.view', $article->slug))->with('success', 'Article successfully created');
    }
    public function update(ArticleRequest $request, $id) {
        $article = Article::find($id);
        if(is_null($article)) return redirect()->back()->with('danger', 'Article not found');

        $article->update($request->except('tags'));

        $this->manageTags($article, $request->get('tags'));
        return redirect(route('articles.view', $article->slug))->with('success', 'Article successfully updated');
    }
    public function delete($id) {
        $article = Article::find($id);
        if(is_null($article)) return redirect()->back()->with('danger', 'Article not found');

        $result = $article->delete();
        if($result) return redirect(route('articles.list'))->with('success', 'Article deleted');
        else return redirect(route('articles.show', $article->slug))->with('danger', 'Failed.');
    }

    private function manageTags($article, $tagString) {
        $allTags = explode(' ', $tagString);
        $existTags = Tag::whereIn('name', $allTags)->get();
        $allTags = collect($allTags);        
        $newTags = $allTags->diff($existTags->pluck('name'));

        $article->tags()->sync($existTags->pluck('id'));

        foreach($newTags as $tagName) {
            $tag = Tag::create(['name' => $tagName]);
            $article->tags()->attach($tag->id);
        }
    }
}
