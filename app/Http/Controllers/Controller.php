<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;

use App\Models\MenuItem;
use App\Models\ArticleLike;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct(Request $request) {        
        View::share('_likes', ArticleLike::where('ip_address', ip2long($request->ip()))->pluck('article_id'));
        View::share('_menuItems', MenuItem::all());        
    }
}
