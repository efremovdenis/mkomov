<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Support\Str;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'slug' => 'required',
            'info' => 'required',
            'body' => 'required',
            'image' => 'required',
            'image_thumb' => ''
        ];
        if($this->has('id')) {
            $rules['slug'] = 'required|'.Rule::unique('articles')->ignore($this->get('id'));
        }
        return $rules;
    }

    public function prepareForValidation() {
        $this->merge([
            'slug' => Str::of($this->get('title'))->slug()
        ]);
    }
}
