<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;
    protected $primaryKey = 'id';
    protected $fillable = ['title', 'slug', 'info', 'body', 'created_at', 'updated_at', 'image', 'image_thumb', 'r_views'];
    protected $table = 'articles';

    public $with = ['views', 'commentaries', 'tags', 'likes'];

    public function getTotalViewsAttribute() {
        return (int) $this->attributes['r_views'] + (int) $this->views->count();
    }
    public function getTotalLikesAttribute() {
        return (int) $this->likes->count();
    }

    public function views() {
        return $this->hasMany(ArticleView::class);
    }
    public function likes() {
        return $this->hasMany(ArticleLike::class);
    } 
    public function commentaries() {
        return $this->hasMany(Commentary::class);
    }
    public function tags() {
        return $this->belongsToMany(Tag::class);
    }

}
