<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleView extends Model
{
    use HasFactory;

    protected $table = 'article_views';
    protected $fillable = ['article_id', 'created_at'];
    const UPDATED_AT = null;        

    public function article() {
        return $this->belongsTo(Article::class);
    }
}
