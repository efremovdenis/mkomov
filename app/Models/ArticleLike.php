<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArticleLike extends Model
{
    use HasFactory;
    protected $table = 'article_likes';
    protected $fillable = ['article_id', 'created_at', 'ip_address'];
    const UPDATED_AT = null;

    public function article() {
        return $this->belongsTo(Article::class);
    }

    public function getIpAddressAttribute($value) {
        return long2ip($value);
    }
    public function setIpAddressAttribute($value) {
        $this->attributes['ip_address'] = ip2long($value);
    }
}
