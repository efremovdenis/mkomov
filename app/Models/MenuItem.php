<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Str;

class MenuItem extends Model {
    protected $fillable = ['name', 'path'];
    public $timestamps = false;

    public function getIsActiveAttribute() {
        if($this->href == url('/')) return (bool) (url()->current() == $this->href);
        return Str::startsWith(url()->current(), $this->href);
    }
    public function getHrefAttribute() {
        return ($this->route == '' ? url($this->path) : url(route($this->route)));
    }
}
