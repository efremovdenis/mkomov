# Install
```
git clone https://efremovdenis@bitbucket.org/efremovdenis/mkomov.git efremovdenis
cd efremovdenis
composer update
```
Then setup ENV file.
```
php artisan migrate
php artisan db:seed
php artisan serve
```

# API endpoints:

## increase views
PUT: api/articles/view 

Required:
- article_id - integer ID

Returns 200 code with new amount
Or 500 if error.

## trigger like
PUT: api/articles/like

Required:
- article_id - integer ID

Returns 200 code with new amount
Or 500 if error.

## store commentary
POST: api/commentaries/store

Required:
- subject - string <= 255
- body - text

Returns:
- 200 if Success
- 422 if Validation error
- 500 if Server Error