<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('admin')->namespace('Admin')->group(function() {
    Route::prefix('articles')->group(function() {
        Route::get('create', 'ArticlesController@create')->name('admin.articles.create');
        Route::post('store', 'ArticlesController@store')->name('admin.articles.store');
        Route::get('{id}/edit', 'ArticlesController@edit')->name('admin.articles.edit');
        Route::get('{id}/delete', 'ArticlesController@delete')->name('admin.articles.delete');
        Route::patch('{id}/update', 'ArticlesController@update')->name('admin.articles.update');
    });
});

Route::get('/', 'ArticlesController@index')->name('articles.index');

Route::get('/articles', 'ArticlesController@list')->name('articles.list');
Route::get('/articles/{slug}', 'ArticlesController@view')->name('articles.view');

Route::get('/tags/{tag}', 'ArticlesController@tag')->name('tags.list');