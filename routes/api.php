<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->namespace('Api')->group(function() {
    Route::prefix('articles')->group(function() {
        Route::put('like', ['as' => 'api.articles.like', 'uses' => 'ArticlesController@like']);
        Route::put('view', ['as' => 'api.articles.view', 'uses' => 'ArticlesController@view']);
    });
    Route::prefix('commentaries')->group(function() {
        Route::post('store', ['as' => 'api.commentaries.store', 'uses' => 'CommentariesController@store']);
    });
});