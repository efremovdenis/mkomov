<nav class="navbar navbar-expand-lg navbar-light">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            @foreach($_menuItems as $item)
                <li class="nav-item {{ $item->isActive ? 'active' : '' }}">
                    <a class="nav-link " href="{{ $item->href }}">
                        {{ $item->name }} 
                        @if($item->isActive)
                            <span class="sr-only">(current)</span>
                        @endif
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</nav>