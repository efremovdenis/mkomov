@foreach(['warning', 'danger', 'success', 'info', 'secondary'] as $label)
    @if(Session::has($label))
        <div class="container">    
            @if(is_array($messages = Session::get($label)))
                @foreach($messages as $message)
                    <div class="alert alert-{{ $label }}" role="alert">
                        {{ $message }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endforeach
            @else
                <div class="alert alert-{{ $label }}" role="alert">
                    {{ $messages }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
    @endif
@endforeach
@if(isset($errors) && $errors->any()) 
    <div class="container">
        @foreach($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endforeach
    </div>
@endif
