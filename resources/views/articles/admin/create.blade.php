@extends('layouts.index')

@section('content')
    <section class="container">
        <div class="row">
            <div class="col">
                <form action="{{ route('admin.articles.store') }}" method="POST">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input class="form-control" type="text" name="title" id="title" value="{{ old('title') }}" placeholder="Type here title" />
                    </div>
                    <div class="form-group"> 
                        <label for="tags">Tags</label>
                        <input class="form-control" type="text" name="tags" id="tags" value="{{ old('tags') }}" placeholder="Type here tags" />
                        <small class="text-muted form-text">Separated by Space</small>
                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <label for="image">Image</label>
                            <input class="form-control" type="text" name="image" id="image" value="{{ old('image') ?? 'https://via.placeholder.com/500x400.png?text=FULL+IMAGE' }}" placeholder="Type here image" />
                        </div>
                        <div class="form-group col">
                            <label for="image_thumb">Image Thumbnail</label>
                            <input class="form-control" type="text" name="image_thumb" id="image_thumb" value="{{ old('image_thumb') ?? 'https://via.placeholder.com/184x184.png?text=IMAGE+THUMB' }}" placeholder="Type here image_thumb" />
                            <small class="text-muted form-text">If empty - it will be shaped from Image</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="info">Info</label>
                        <textarea class="form-control" name="info" id="info" rows="10">{{ old('info') }}</textarea>
                        <small class="text-muted form-text">Short information about article</small>
                    </div>
                    <div class="form-group">
                        <label for="body">Body</label>
                        <textarea class="form-control" name="body" id="body" rows="20">{{ old('body') }}</textarea>
                        <small class="text-muted form-text">Full content</small>
                    </div>
                    <div class="form-group">
                    {{ csrf_field() }}
                    <button class="btn btn-success" type="submit">Save</button></div>
                </form>
            </div>
        </div>
    </section>
@endsection