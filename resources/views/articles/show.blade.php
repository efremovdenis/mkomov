@extends('layouts.index')

@section('content')
    <div class="container">
        <h3>{{ $article->title }} <a href="{{ route('admin.articles.edit', $article->id) }}" class="btn btn-sm btn-warning float-right">Edit article</a><a href="{{ route('admin.articles.delete', $article->id) }}" class="btn btn-sm btn-danger float-right mx-2 confirm-link">Delete article</a></h3>
        <p>
            <img src="{{ $article->image }}" alt="" class="float-left p-2" />
            {{ $article->body }}
        </p>
        <div class="clearfix"></div>
        <hr />
        <div class="row">
            <p class="col-2">Views: <span id="total-views">{{ $article->total_views }}</span></p>
            <p class="col-2">Likes: 
                @if($_likes->contains($article->id))
                    <button type="button" class="btn btn-like btn-sm btn-danger" data-article-id="{{ $article->id }}">{{ $article->total_likes }} Likes</button>
                @else
                    <button type="button" class="btn btn-like btn-sm btn-outline-secondary" data-article-id="{{ $article->id }}">{{ $article->total_likes }} Likes</button>
                @endif
            </p>
            <p class="col">Tags: 
                @foreach($article->tags as $tag)
                    <a href="{{ route('tags.list', $tag->name) }}"><span class="badge bg-secondary badge-pill text-white">{{ $tag->name }}</span></a>
                @endforeach
            </p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col">
                <h4 class="border-bottom border-gray pb-2 mb-0">Commentaries</h4>
                <div id="commentary-list">
                    @if($article->commentaries->isEmpty())
                        <h6 id="no-commentaries" class="text-muted py-3">No commentaries yet</h6>
                    @endif
                    @foreach($article->commentaries as $commentary)
                        <div class="media text-muted pt-3">
                            <svg class="bd-placeholder-img mr-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: 32x32" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#007bff"></rect><text x="50%" y="50%" fill="#007bff" dy=".3em">32x32</text></svg>
                            <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                                <div class="d-flex justify-content-between align-items-center w-100">
                                    <strong class="text-gray-dark">{{ $commentary->subject }}</strong>
                                </div>
                                <span class="d-block">{{ $commentary->body }}</span>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col">
                <h4>Send your commentary</h4>
                <div>
                    <form action="{{ route('api.commentaries.store') }}" method="POST" id="commentary-form">
                        <div id="overlay"><div id="overlay-content"><div id="loader-image"></div></div></div>
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" name="subject" id="subject" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label for="body">Body</label>
                            <textarea name="body" id="body" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="article_id" id="article_id" value="{{ $article->id }}" />
                            <button class="btn btn-success" type="button" id="commentary-form-submit">Store</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('head')
<style>
    #loader-image{
        position: relative;
        width: 50px;
        height: 50px;
        border: 2px solid rgba(255,255,255,0.2);
        border-radius: 50px;
        top:135px;
        left:50%;
        margin-left: -25px;
        animation-name: spinner 0.4s linear infinite;
        -webkit-animation: spinner 0.4s linear  infinite;
        -moz-animation: spinner 0.4s linear  infinite; 
        }
    #loader-image:before{
        position: absolute;
        content:'';
        display: block;
        background-color: rgba(0,0,0,0.2);
        width: 80px;
        height: 80px;
        border-radius: 80px;
        top: -15px;
        left: -15px;
        }
    #loader-image:after{
        position: absolute;
        content:'';
        width: 50px;
        height: 50px;
        border-radius: 50px;
        border-top:2px solid white;
        border-bottom:2px solid white;
        border-left:2px solid white;
        border-right:2px solid transparent;
        top: -2px;
        left: -2px;
        }

    @keyframes spinner{
        from {transform: rotate(0deg);}
        to {transform: rotate(360deg);}
    }

    @-webkit-keyframes spinner{
        from {transform: rotate(0deg);}
        to {transform: rotate(360deg);}
    }

    @-moz-keyframes spinner{
        from {transform: rotate(0deg);}
        to {transform: rotate(360deg);}
    }
    #overlay{position: absolute;left: 0; top: 0; right: 0; bottom: 0;z-index: 2;background-color: rgba(255,255,255,0.8);}
    #overlay-content {
        position: absolute;
        transform: translateY(-50%);
        -webkit-transform: translateY(-50%);
        -ms-transform: translateY(-50%);
        top: 0;
        left: 0;
        right: 0;
        text-align: center;
        color: #555;
    }

</style>
@endsection
@section('js')
<script type="text/javascript">
function update_views() {
    $.ajax({
        type: 'PUT',
        url: '{{ route('api.articles.view') }}',
        data: {article_id: {{ (int) $article->id }} }
    }).fail(function(data) {
        alert("System error while updating views");
    }).done(function(data) {
        $("#total-views").text(data);
    });
}
function print_commentary(subject, body) {
    let html = "";
    html += '<div class="media text-muted pt-3">';
    html += '    <svg class="bd-placeholder-img mr-2 rounded" width="32" height="32" xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: 32x32" preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title><rect width="100%" height="100%" fill="#007bff"></rect><text x="50%" y="50%" fill="#007bff" dy=".3em">32x32</text></svg>';
    html += '    <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">';
    html += '        <div class="d-flex justify-content-between align-items-center w-100">';
    html += '            <strong class="text-gray-dark">' + subject +'</strong>';
    html += '        </div>';
    html += '        <span class="d-block">' + body + '</span>';
    html += '    </div>';
    html += '</div>';
    $("#commentary-list").append(html);
}
/* Every 5 secs send AJAX to increase Article Views */
$(document).ready(setTimeout(update_views, 5000));
$("#overlay").hide();
$(".confirm-link").on('click', function(e) {
    return confirm('Are you sure?');
});
var requestInProgress = false;
$("#commentary-form-submit").on('click', function(e) {    
    if(requestInProgress) return; // Uses to stop copy request with same data.
    requestInProgress = true;
    $("#overlay").show(200);

    let formData = $("#commentary-form").serialize();
    $.ajax({
        type: 'POST',
        url: '{{ route('api.commentaries.store') }}',
        data: formData
    }).done(function(data) {
        alert('Commentary stored');
        print_commentary($("#subject").val(), $("#body").val());
        $('.form-control').val('');
        $("#no-commentaries").remove();

        requestInProgress = false;
        $("#overlay").hide();
    }).fail(function(data) {
        if(data.status == 422) { // If Validation error -> Show errors for each field.
            alert(data.responseJSON.message);
            Object.values(data.responseJSON.errors).forEach((e) => {alert(e);});
        }
        else {
            alert('Failed: System error. Try again later.');
        }
        requestInProgress = false;
        $("#overlay").hide(200);
    });
});
</script>
@endsection