@extends('layouts.index')

@section('content')
<div class="album py-5 bg-light">
    <div class="container">
        <div class="row">
            <h2>{{ $title }}</h2>
            @foreach($articles as $article)
                <div class="col-md-12">
                    <div class="card mb-4 shadow-sm">
                        <div class="row no-gutters">
                            <div class="col-2">
                                <img src="{{ $article->image_thumb }}" />
                            </div>
                            <div class="col-10">
                                <div class="card-body">
                                    <h5 class="card-title"><a href="{{ route('articles.view', $article->slug) }}">{{ $article->title }}</a></h5>
                                    <p class="card-text">{{ $article->info }}</p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                        @if($_likes->contains($article->id))
                                            <button type="button" class="btn btn-like btn-sm btn-danger" data-article-id="{{ $article->id }}">{{ $article->total_likes }} Likes</button>
                                        @else
                                            <button type="button" class="btn btn-like btn-sm btn-outline-secondary" data-article-id="{{ $article->id }}">{{ $article->total_likes }} Likes</button>
                                        @endif
                                        </div>
                                        <small class="text-muted">{{ $article->total_views }} views</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            @endforeach        
        </div>
        <div class="row">
            <div class="text-center mx-auto">
                {{ $articles->links() }}
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')

@endsection