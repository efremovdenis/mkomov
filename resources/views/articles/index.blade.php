@extends('layouts.index')

@section('content')
<section class="jumbotron text-center my-0">
    <div class="container">
        <h1>Album example</h1>
        <p class="lead text-muted">Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don’t simply skip over it entirely.</p>
        <p>
            <a href="#" class="btn btn-primary my-2">Main call to action</a>
            <a href="#" class="btn btn-secondary my-2">Secondary action</a>
        </p>
    </div>
</section>
<div class="album py-5 bg-light">
    <div class="container">
        <h2 class="text-muted mb-5">Our last posts:</h2>
        <div class="row">
            @foreach($articles as $article)
                <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                        <img src="{{ $article->image_thumb }}" alt="" />
                        <div class="card-body">
                            <p class="card-text"><a href="{{ route('articles.view', $article->slug) }}">{{ $article->title }}</a></p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    @foreach($article->tags->take(2) as $tag)
                                        <button type="button" class="badge badge-pill text-white mr-1">{{ $tag->name }}</button>
                                    @endforeach
                                </div>
                            <small class="text-muted">{{ $article->total_views }} Views</small>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endsection