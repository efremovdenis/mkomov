<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\MenuItem;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        ini_set('memory_limit', -1);
        \App\Models\Article::factory(20)->create();
        \App\Models\ArticleView::factory(100)->create();
        \App\Models\Tag::factory(25)->create();        

        for($a = 1; $a <= 20; $a++) {
            $tags = [];
            for($i = 1; $i < rand(5,10); $i++) {
                if(rand(0,1)) {
                    $tags[] = rand(1,25);
                }
            }
            Article::find($a)->tags()->sync(array_unique($tags));
        }

        $menuItems = [[
            'name' => 'Home',
            'route' => 'articles.index'
        ],[
            'name' => 'Articles',
            'route' => 'articles.list'
        ]];

        foreach($menuItems as $row) MenuItem::create($row);
    }
}
