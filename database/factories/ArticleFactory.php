<?php

namespace Database\Factories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;

class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(4),
            'slug' => $this->faker->unique()->slug(4),
            'info' => $this->faker->text(),
            'body' => $this->faker->text(1000),
            'image' => 'https://via.placeholder.com/500x400.png?text=FULL+IMAGE',
            'image_thumb' => 'https://via.placeholder.com/184x184.png?text=IMAGE+THUMB',

            'r_views' => $this->faker->numberBetween(100, 1000)
        ];
    }
}
