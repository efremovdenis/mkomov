<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_likes', function (Blueprint $table) {
            $table->foreignId('article_id');
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
            // $table->integer('user_id'); // - If APP use Users
            $table->integer('ip_address'); // - BASE thing to separate users. May be with User Agents and so on.
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article_likes');
    }
}
